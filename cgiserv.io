CGIReq := Object clone do(
  handleSocket := method(aSocket,
    aSocket streamReadNextChunk
    
    request := aSocket readBuffer betweenSeq("GET ", " HTTP")
    uri := URL with("http://example.net#{request}" interpolate)
    
    cgi := CGI2 clone
    cgi setStandardIO(aSocket)
    cgi setRequestMethod("GET")
    cgi setQueryString(uri query)
    hash := cgi parse
    
    aSocket streamWrite("<html><head><title>Io!</title></head><body><h1>IO!</h1><p>")
    hash foreach(k,v, aSocket streamWrite("#{k} - #{v} <br/>" interpolate))
    aSocket streamWrite("</p></body></html>")
    
    aSocket close
  )
)

CGIServ := Server clone do(
    setPort(8000)
    handleSocket := method(aSocket,
        CGIReq clone @handleSocket(aSocket)
    )
)
 
CGIServ start